#!/bin/bash

# assumes the repo was cloned to ~/.dotfiles

rm $HOME/.bashrc
rm $HOME/.inputrc
rm $HOME/.gitconfig

ln -s $(pwd)/bashrc $HOME/.bashrc
ln -s $(pwd)/inputrc $HOME/.inputrc
ln -s $(pwd)/gitconfig $HOME/.gitconfig

exit 0
