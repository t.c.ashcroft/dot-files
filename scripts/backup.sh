#!/bin/bash

cd /home

tar -cvpzf backup.tar.gz --exclude=/home/tashcrof/Programs --exclude=/home/tashcrof/bin --exclude=/home/tashcrof/.cache --exclude=/home/tashcrof/.config --exclude=/home/tashcrof/.steam* --exclude=/home/tashcrof/Downloads --exclude=/home/tashcrof/snap --exclude=/home/tashcrof/.android --exclude=/home/tashcrof/workspace/android --exclude=/home/tashcrof/workspace/docker/oracle-images --exclude=/home/tashcrof/.local --exclude=/home/tashcrof/.m2 --exclude=/home/tashcrof/.IntelliJ* --exclude=/home/tashcrof/.WebStorm* --exclude=/home/tashcrof/.mozilla --exclude=/home/tashcrof/.gradle --exclude=/home/tashcrof/.wine --one-file-system /home/tashcrof

