#!/bin/bash

if [[ ! -z $(pgrep eclim) ]]
then
  echo "Eclim is running"
else 
  nohup eclimd &>~/log/eclim.log &
fi
