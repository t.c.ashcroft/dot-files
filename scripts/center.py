#!/usr/bin/python

import sys


def get_middle_character(odd_string):
    variable = len(odd_string)
    x = str((variable/2))
    middle_character = odd_string.find(x)
    middle_character2 = odd_string[middle_character]
    return middle_character2


str1 = sys.argv[1]
str2 = sys.argv[2]

if len(str1) > len(str2):
    diff = len(str1) - len(str2)
    # print("A - diff: " + str(diff))
    val = str1 + ' | ' + str2.rjust(len(str2) + diff, ' ')
    print(get_middle_character(val))
    print(val)
elif len(str2) > len(str1):
    diff = len(str2) - len(str1)
    # print("B - diff: " + str(diff))
    val = str1.rjust(len(str1) + diff, ' ') + ' | ' + str2
    print(get_middle_character(val))
    print(val)
else:
    # print("C")
    print(str1 + " | " + str2)


