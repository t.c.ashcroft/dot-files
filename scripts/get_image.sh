#! /bin/bash

DOCKED="$(cat /home/tashcrof/.screenlayout/docked)"

if [ "$DOCKED" -eq "1" ] 
then
	# Return a big picture
	echo "$(ls /home/tashcrof/Pictures/big/*.png | sort -R | tail -1)"
else
	# Return a small picture
	echo "$(ls /home/tashcrof/Pictures/small/*.png | sort -R | tail -1)"
fi
